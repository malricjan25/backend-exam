<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['namespace' => 'Api'], function(){

    /**
     * ***** [AUTHENTICATION ROUTES] *****
     */
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');

    /**
     * ***** [POST'S COMMENTS ROUTES] *****
     */
    Route::get('posts/{post}/comments', 'CommentsController@index');
    Route::post('posts/{post}/comments', 'CommentsController@store');
    Route::patch('posts/{post}/comments/{comment}', 'CommentsController@update');
    Route::delete('posts/{post}/comments/{comment}', 'CommentsController@destroy');


    /**
     * ***** [POSTS ROUTES] *****
     */
    Route::resource('posts', 'PostsController');

});