<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LoginRequest;
use App\Http\Requests\Api\UserRequest;
use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    public function dateFormat($date)
    {
        return date( "Y-m-d H:i:s", strtotime($date) );
    }

    public function register(UserRequest $request)
    {
        $request->validated();

        $user = new User();

        $user->newUser($request);
        $token = $user->createToken('authToken');

        $response = [
            'token' => $token->accessToken,
            'token_type' => 'bearer',
            'expires_at' => $this->dateFormat($token->token->expires_at)
        ];

        return response()->json($response);
    }
    
    public function checkEmailIfExists($email)
    {
        return User::whereEmail($email)->exists();
    }

    public function login(LoginRequest $request)
    {
        $request->validated();
        $credentials = request(['email', 'password']);
        
        auth()->attempt($credentials);
        $token = auth()->user()->createToken('authToken');
        $response = [
            'token' => $token->accessToken,
            'token_type' => 'bearer',
            'expires_at' => $this->dateFormat($token->token->expires_at)
        ];

        return response()->json($response);
        
    }
    public function logout()
    {
        auth()->user()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }
}