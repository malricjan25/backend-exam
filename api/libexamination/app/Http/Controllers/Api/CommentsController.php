<?php

namespace App\Http\Controllers\Api;

use App\Comment;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CommentRequest;
use App\Http\Resources\Comments\CommentResource;
use App\Http\Resources\Comments\CommentsResource;
use App\Post;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Post $post)
    {
        $comments = $post->comments;
        return new CommentsResource($comments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request, Post $post)
    {
        $user = auth('api')->user();
        $data = $post->newComment($request->body, $user);
        return new CommentResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommentRequest $request, Post $post, Comment $comment)
    {
        $data = $post->comments->where('id', $comment->id)->first();
        $data->update(['body' => $request->body]);

        return new CommentResource($data);
    }
        

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post, Comment $comment)
    {
        $user = auth('api')->user();
        if($user->id != $comment->user->id)
        {
            return response()->json(['error' => 'You can\'t delete this comment!']);
        }
        $comment->delete();
        return response()->json(['status' => 'Record deleted successfully']);
    }
}
