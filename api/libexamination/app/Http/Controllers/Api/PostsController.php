<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\PostRequest;
use App\Http\Resources\Posts\PostResource;
use App\Http\Resources\Posts\PostsResource;
use App\Post;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::paginate(1);

        return new PostsResource($posts);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return $post;
    }

    public function store(PostRequest $request)
    {
        $user = auth('api')->user();
        $post = new Post();

        return new PostResource($post->persists($request, $user));
    }

    public function update(PostRequest $request, Post $post)
    {
        $user = auth('api')->user();
        return new PostResource($post->persists($request, $user));
    }

    public function destroy(Post $post)
    {
        $user = auth('api')->user();
        if($user->id != $post->user->id)
        {
            return response()->json(['error' => 'You can\'t delete this post!']);
        }
        $post->delete();
        return response()->json(['status' => 'Record deleted successfully']);
    }
}
