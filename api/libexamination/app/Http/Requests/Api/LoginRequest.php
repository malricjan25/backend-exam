<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            'email' => 'required|exists:users,NULL',
            'password' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'email.exists' => 'These credentials do not match our records.'
        ];
    }
    
    public function response(array $errors)
    {
        return new JsonResponse(['error' => $errors], 400);
    }
}
