<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    protected $rules = [
        'POST' => [
            'title' => 'required|unique:posts|min:2|max:50',
            'content' => 'required|min:2|max:255',
        ],
        'PATCH' => [
            'content' => 'required|min:2|max:255'
        ],
        'PUT' => [
            'content' => 'required|min:2|max:255'
        ]
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->request->add(['slug' => str_slug($this->title)]);

        if($this->method() == 'PUT' || $this->method() == 'PATCH')
        {
            return [
                'title' => 'sometimes|min:2|max:50|unique:posts,title,' . $this->post->id,
            ];
        }

        return $this->rules[$this->method()];
    }

    public function response(array $errors)
    {
        return new JsonResponse(['error' => $errors], 400);
    }
}
