<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['user_id', 'title', 'slug', 'content'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * Function to persists posts either Update or Create
     * @param Request $request
     * @param App\User $user
     * 
     * @return Post $post
     */
    public function persists($request, $user)
    {
        $post = $this->updateOrCreate(
            ['id' => $this->id],
            [
                'title' => $request->title,
                'slug' => $request->slug,
                'content' => $request->content,
                'user_id' => $user->id
            ]
        );

        return $post;
    }

    /**
     * Function for creating new comment for Post
     * @param $body, 
     * @param App\User $user
     * 
     * @return Comment $data
     */
    public function newComment($body, $user)
    {
        $data = $this->comments()->create([
            'body' => $body,
            'user_id' => $user->id
        ]);
        return $data;
    }
}
