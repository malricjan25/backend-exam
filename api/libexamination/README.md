# Project Title

LIB BACKEND EXAMINATION.

## Technologies and Packages used

Below are the details of the system based on the given API Documentation.

## Built With

* [Laravel](https://laravel.com/) - The web framework used
* [Postman](https://www.getpostman.com/) -  The most popular tools used in API testing
* [Laragon](https://laragon.org/) - Universal development environment for PHP, Node. js, Python, Java, Go, Ruby


## Routes

```python
routes/api.php

Route::group(['namespace' => 'Api'], function(){

    /**
     * ***** [AUTHENTICATION ROUTES] *****
     */
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');

    /**
     * ***** [POST'S COMMENTS ROUTES] *****
     */
    Route::get('posts/{post}/comments', 'CommentsController@index');
    Route::post('posts/{post}/comments', 'CommentsController@store');
    Route::patch('posts/{post}/comments/{comment}', 'CommentsController@update');
    Route::delete('posts/{post}/comments/{comment}', 'CommentsController@destroy');


    /**
     * ***** [POSTS ROUTES] *****
     */
    Route::resource('posts', 'PostsController');

});
```

## Time Spent
CODING STARTED 09/01/2020 10:12 PM
| CODING ENDED   10/01/2020 04:06 AM


#### Powered By
@fingersandmind